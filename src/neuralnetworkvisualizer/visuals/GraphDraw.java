/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetworkvisualizer.visuals;

import java.util.HashMap;
import java.util.List;
import neuralnetworkvisualizer.networks.NeuralNetwork;
import neuralnetworkvisualizer.neurons.NeuralLayer;
import org.graphstream.graph.*;
import org.graphstream.graph.implementations.*;
import org.graphstream.ui.view.Viewer;

public class GraphDraw {

    final int height = 720;
    final int width = 1280;
    final int x_spacing = 30;
    final int y_spacing = 30;
    final int node_size = 10;
    int max_rows;
    Graph graph;
    Viewer viewer;

    public GraphDraw() {
        this.graph = new MultiGraph("Visualization");
        System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");

    }

    public void createGraph(NeuralNetwork net) {
        List<NeuralLayer> layers = net.getLayers();
        int x_offset = (this.width - this.x_spacing * 2) / layers.size();
        this.max_rows = this.getMaxNeurons(layers);
        for (NeuralLayer l : layers) {
            char index = 'a';
            int layer_x = x_offset * (l.getId() + 1) - this.x_spacing;
            for (int i = 0; i < l.size(); i++) {
                int y_offset = (this.height - y_spacing * 10) / l.size();
                int layer_y = y_offset * (i + 1) - y_offset / 2;
                String current_id = String.valueOf(l.getId()) + String.valueOf(index);
                this.addNeuron(current_id, layer_x, layer_y);
                index++;
            }
        }

        for (NeuralLayer l : layers) {
            char index = 'a';
            for (int i = 0; i < l.size(); i++) {
                HashMap<HashMap<Integer, Integer>, Double> conns = l.getNeuron(i).getConnections();
                String current_id = String.valueOf(l.getId()) + String.valueOf(index);
                index++;
                for (HashMap<Integer, Integer> m : conns.keySet()) {
                    for (Integer neur_index : m.keySet()) {
                        Integer layer_index = m.get(neur_index);
                        char chni = (char) ((char) neur_index.intValue() + 'a');

                        String to_id = String.valueOf(layer_index) + String.valueOf(chni);// String.valueOf(('a'+neur_index));
                        this.addConnection(current_id, to_id, conns.get(m));
                    }
                }

            }
        }
    }

    public void updateGraph(NeuralNetwork net) {
        List<NeuralLayer> layers = net.getLayers();
        for (NeuralLayer l : layers) {
            char index = 'a';
            for (int i = 0; i < l.size(); i++) {
                HashMap<HashMap<Integer, Integer>, Double> conns = l.getNeuron(i).getConnections();
                String current_id = String.valueOf(l.getId()) + String.valueOf(index);
                index++;
                Double s_val = 30. + l.getNeuron(i).getValue();
                if (s_val > 60) {
                    s_val = 60.;
                }
                if (s_val < 15) {
                    s_val = 15.;
                }
                String size_val = "size: " + String.valueOf(s_val) + "px;";
                this.graph.getNode(current_id).changeAttribute("ui.style", size_val);
                for (HashMap<Integer, Integer> m : conns.keySet()) {
                    for (Integer neur_index : m.keySet()) {
                        Integer layer_index = m.get(neur_index);
                        char chni = (char) ((char) neur_index.intValue() + 'a');

                        String to_id = String.valueOf(layer_index) + String.valueOf(chni);// String.valueOf(('a'+neur_index));
                        String edge_id = current_id + to_id;
                        String str_weight = String.format("%.2f", conns.get(m));
                        this.graph.getEdge(edge_id).changeAttribute("ui.label", str_weight);
                    }
                }

            }
        }
    }

    private int getMaxNeurons(List<NeuralLayer> layers) {
        int max = -1;
        for (NeuralLayer l : layers) {
            if (layers.size() > max) {
                max = layers.size();
            }
        }
        return max;
    }

    public void display() {
        viewer = this.graph.display(false);
        viewer.disableAutoLayout();
        viewer.getDefaultView().resizeFrame(this.width, this.height);
        viewer.setCloseFramePolicy(Viewer.CloseFramePolicy.CLOSE_VIEWER);

        for (Node n : this.graph.getNodeSet()) {
            n.addAttribute("ui.style", "fill-color: rgb(0,100,255);");
        }

    }

    private void addNeuron(String id, int x, int y) {
        Node n = this.graph.addNode(id);
        n.addAttribute("ui.label", id);
        n.addAttribute("ui.style", "size: 15px;");
        n.setAttribute("x", x);
        n.setAttribute("y", y);
    }

    private void addConnection(String from, String to, Double weight) {
        String edgename = from + to;
        String str_weight = String.format("%.2f", weight);
        Edge e = this.graph.addEdge(edgename, from, to, true);
        int xdist = (int) e.getTargetNode().getAttribute("x") - (int) e.getSourceNode().getAttribute("x");
        int ydist = (int) e.getTargetNode().getAttribute("y") - (int) e.getSourceNode().getAttribute("y");
        double edgelen = Math.sqrt(Math.pow((double) xdist, 2.) + Math.pow((double) ydist, 2.));
        String t_off = String.format("text-offset: %d,%d;", (int) (edgelen / 3) * -1, 0);
        String type_align = "along";
        if (from.equals(to)) {
            t_off = "";
            type_align = "center";
        }
        e.addAttribute("ui.label", String.valueOf(str_weight));
        e.addAttribute("ui.style", "fill-color: rgb(120,120,120); text-alignment: " + type_align + "; text-background-mode: plain;" + t_off);
    }

}

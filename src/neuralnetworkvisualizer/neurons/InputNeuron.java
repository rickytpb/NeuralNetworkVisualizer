/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetworkvisualizer.neurons;

import java.util.HashMap;

/**
 *
 * @author Jacek
 */
public class InputNeuron extends Neuron {

    public InputNeuron() {
        super();
    }

    public Double getOutput(Integer neur_index, Integer layer_index) {
        HashMap<Integer, Integer> conn = new HashMap<Integer, Integer>();
        conn.put(neur_index, layer_index);
        Double x = this.value * this.getWeight(conn);
        return x;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetworkvisualizer.neurons;

import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import neuralnetworkvisualizer.neurons.Neuron;
import java.io.Serializable;

/**
 *
 * @author Jacek
 */
public abstract class NeuralLayer implements Serializable {

    protected int id;
    protected List<Neuron> neurons;
    protected Double bias;

    public NeuralLayer() {
        this.neurons = new ArrayList<Neuron>(Collections.nCopies(1, new Neuron()));
        this.bias = 1.;
        this.id = -1;
    }

    public int getId() {
        return this.id;
    }

    public NeuralLayer(int neurons) {
        this.neurons = new ArrayList<Neuron>();
        this.bias = 1.;
        for (int i = 0; i < neurons; i++) {
            this.neurons.add(new Neuron());
        }
    }

    public Neuron getNeuron(int index) {
        return this.neurons.get(index);
    }

    public int size() {
        return this.neurons.size();
    }

    public void connect(int index, int neur_index, int layer_index) {
        this.neurons.get(index).addConnect(neur_index, layer_index);
    }

    public void connectFull(NeuralLayer next) {
        for (Neuron n : this.neurons) {
            for (int i = 0; i < next.size(); i++) {
                n.addConnect(i, next.id);
            }
        }
    }

    public void connectSelf() {
        for (int i = 0; i < this.neurons.size(); i++) {
            this.neurons.get(i).addConnect(i, this.id);
        }
    }

    public void clearValues() {
        for (Neuron n : this.neurons) {
            n.setValue(0.);
        }
    }

    public void print() {
        System.out.println("Printing neuron values and weights");
        for (Neuron n : this.neurons) {
            System.out.println(n.getValue());
            System.out.println(n.getConnections());
        }
    }

}

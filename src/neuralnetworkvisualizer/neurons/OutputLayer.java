/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetworkvisualizer.neurons;

/**
 *
 * @author Jacek
 */
public class OutputLayer extends NeuralLayer {

    public OutputLayer(int id) {
        super();
        this.id = id;
    }

    public Double getPrediction() {
        return this.getNeuron(0).getBinarized();
    }

    public Double getLinearPrediction() {
        return this.getNeuron(0).value;
    }

}

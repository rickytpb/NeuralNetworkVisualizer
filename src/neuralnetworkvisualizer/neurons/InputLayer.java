/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetworkvisualizer.neurons;

import java.util.ArrayList;

/**
 *
 * @author Jacek
 */
public class InputLayer extends NeuralLayer {

    public InputLayer(int neurons) {
        this.id = 0;
        this.neurons = new ArrayList<Neuron>();
        this.bias = 1.;
        for (int i = 0; i < neurons; i++) {
            this.neurons.add(new InputNeuron());
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetworkvisualizer.neurons;

import java.util.HashMap;
import java.io.Serializable;

/**
 *
 * @author Jacek
 */
public class Neuron implements Serializable {

    private HashMap<HashMap<Integer, Integer>, Double> connections;
    protected transient Double value;

    public Neuron() {
        this.connections = new HashMap<HashMap<Integer, Integer>, Double>();
        this.value = 0.;
    }

    public int size() {
        return this.connections.size();
    }

    protected Double getWeight(HashMap<Integer, Integer> index) {
        return this.connections.get(index);
    }

    public void updateWeight(int neuron, int layer, Double value) {
        HashMap<Integer, Integer> conn = new HashMap<>();
        conn.put(neuron, layer);
        this.connections.replace(conn, this.connections.get(conn) + value);
    }

    public Double getOutput(Integer neur_index, Integer layer_index) {
        HashMap<Integer, Integer> conn = new HashMap<>();
        conn.put(neur_index, layer_index);
        Double x = this.value * this.getWeight(conn);
        if (x >= 0) {
            return 1.;
        } else {
            return 0.;
        }
    }

    public Double getBinarized() {
        if (this.value > 0) {
            return 1.;
        } else {
            return 0.;
        }
    }

    public void addConnect(int neur_index, int layer_index) {
        HashMap<Integer, Integer> conn = new HashMap<Integer, Integer>();
        conn.put(neur_index, layer_index);
        this.connections.put(conn, 0.);
    }

    public HashMap<HashMap<Integer, Integer>, Double> getConnections() {
        return this.connections;
    }

    public void setValue(Double val) {
        this.value = val;
    }

    public Double getValue() {
        return this.value;
    }
}

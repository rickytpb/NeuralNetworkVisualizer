/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetworkvisualizer.networks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import neuralnetworkvisualizer.interfaces.Model;
import neuralnetworkvisualizer.neurons.NeuralLayer;
import neuralnetworkvisualizer.neurons.Neuron;
import neuralnetworkvisualizer.utils.DataObject;

/**
 *
 * @author Jacek
 */
public class AdaptivePerceptron extends NeuralNetwork implements Model{

    public List<Double> weights;
    public Integer errors;
    public List<Double> error_list;

    private AdaptivePerceptron(AdaptivePerceptronBuilder builder) {
        this.layers = builder.layers;
        this.n_iter = builder.n_iter;
        this.eta = builder.eta;
        this.randomWeights(-1., 1.);
    }

    @Override
    public void fit(DataObject src, DataObject dst) {
        for (int iter = 0; iter < this.n_iter; iter++) {
            for (int i = 0; i < src.getCount(); i++) {
                this.clearLayersValues();
                Double error = (Double) dst
                        .getFrameByIndex(i)
                        .getValueByIndex(0) - this.getPredict(src.getFrameByIndex(i));
                error = error * this.eta;
                this.inputInit(src.getFrameByIndex(i));
                for (int layerno = 0; layerno < this.layers.size(); layerno++) {
                    for (int neuronno = 0; neuronno < this.layers.get(layerno).size(); neuronno++) {
                        Neuron current_neuron = this.layers.get(layerno).getNeuron(neuronno);
                        HashMap<HashMap<Integer, Integer>, Double> conns = current_neuron.getConnections();
                        for (HashMap<Integer, Integer> index : conns.keySet()) {
                            for (Integer connneur : index.keySet()) {
                                Integer layer = index.get(connneur);
                                current_neuron.updateWeight(connneur, layer, error * current_neuron.getValue());
                                Double output = current_neuron.getOutput(connneur, layer);
                                Neuron connected = this.layers.get(layer).getNeuron(connneur);
                                connected.setValue(connected.getValue() + output);
                            }
                        }
                    }
                }
                if (this.visualize) {
                    try {
                        Thread.sleep(this.visual_sleep);
                    } catch (InterruptedException e) {
                        System.err.println("There is a problem with neural thread");
                    }

                }
            }
        }
        this.clearLayersValues();
    }

    public static class AdaptivePerceptronBuilder extends NetworkBuilder {

        private ArrayList<NeuralLayer> layers;
        private int n_iter;
        private Double eta;

        public AdaptivePerceptronBuilder(Double eta, int n_iter) {
            super(eta, n_iter);
            this.layers = super.layers;
            this.eta = super.eta;
            this.n_iter = super.n_iter;

        }

        public AdaptivePerceptronBuilder addInputLayer(int nodes) {
            return (AdaptivePerceptronBuilder) super.addInputLayer(nodes);
        }

        public AdaptivePerceptronBuilder addOutputLayer() {
            return (AdaptivePerceptronBuilder) super.addOutputLayer();
        }

        public AdaptivePerceptronBuilder connectFully() {
            for (int i = 0; i < this.layers.size() - 1; i++) {
                this.layers.get(i).connectFull(this.layers.get(i + 1));
            }
            return this;
        }

        public AdaptivePerceptron build() {
            return new AdaptivePerceptron(this);
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetworkvisualizer.networks;

import java.util.ArrayList;
import neuralnetworkvisualizer.neurons.NeuralLayer;
import neuralnetworkvisualizer.neurons.HiddenLayer;
import neuralnetworkvisualizer.neurons.InputLayer;
import neuralnetworkvisualizer.neurons.OutputLayer;

/**
 *
 * @author Jacek
 */
public abstract class NetworkBuilder {

    protected ArrayList<NeuralLayer> layers;
    protected int n_iter;
    protected Double eta;

    public NetworkBuilder(Double eta, int n_iter) {
        this.eta = eta;
        this.n_iter = n_iter;
        this.layers = new ArrayList<NeuralLayer>();
    }

    public NetworkBuilder addInputLayer(int nodes) {
        this.layers.add(new InputLayer(nodes));
        return this;
    }

    public NetworkBuilder addHiddenLayer(int nodes) {
        int id = this.layers.size();
        this.layers.add(new HiddenLayer(nodes, id));
        return this;
    }

    public NetworkBuilder addOutputLayer() {
        int id = this.layers.size();
        this.layers.add(new OutputLayer(id));
        return this;
    }

    public abstract NetworkBuilder connectFully();

    public abstract NeuralNetwork build();
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetworkvisualizer.networks;

import java.util.ArrayList;
import neuralnetworkvisualizer.interfaces.Model;
import neuralnetworkvisualizer.neurons.NeuralLayer;
import neuralnetworkvisualizer.networks.NetworkBuilder;

/**
 *
 * @author Jacek
 */
public class RecurrentNetwork extends NeuralNetwork  implements Model{

    private RecurrentNetwork(RecurrentBuilder builder) {
        this.layers = builder.layers;
        this.n_iter = builder.n_iter;
        this.eta = builder.eta;
        this.randomWeights(-1., 1.);
    }

    public static class RecurrentBuilder extends NetworkBuilder {

        private ArrayList<NeuralLayer> layers;
        private int n_iter;
        private Double eta;

        public RecurrentBuilder(Double eta, int n_iter) {
            super(eta, n_iter);
            this.layers = super.layers;
            this.eta = super.eta;
            this.n_iter = super.n_iter;
        }

        public RecurrentBuilder addInputLayer(int nodes) {
            return (RecurrentBuilder) super.addInputLayer(nodes);
        }

        public RecurrentBuilder addHiddenLayer(int nodes) {
            return (RecurrentBuilder) super.addHiddenLayer(nodes);
        }

        public RecurrentBuilder addOutputLayer() {
            return (RecurrentBuilder) super.addOutputLayer();
        }

        public RecurrentBuilder connectFully() {
            for (int i = 0; i < this.layers.size() - 1; i++) {
                this.layers.get(i).connectFull(this.layers.get(i + 1));
                if (i > 0) {
                    this.layers.get(i).connectSelf();
                }
                if (i > 1) {
                    this.layers.get(i + 1).connectFull(this.layers.get(i));
                }
            }
            return this;
        }

        public RecurrentNetwork build() {
            return new RecurrentNetwork(this);
        }

    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetworkvisualizer.networks;

import neuralnetworkvisualizer.neurons.OutputLayer;
import neuralnetworkvisualizer.neurons.NeuralLayer;
import neuralnetworkvisualizer.neurons.Neuron;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import neuralnetworkvisualizer.utils.DataObject;
import neuralnetworkvisualizer.utils.DataFrame;
import java.util.HashMap;
import java.io.Serializable;

/**
 *
 * @author Jacek
 */
public abstract class NeuralNetwork implements Serializable {

    protected ArrayList<NeuralLayer> layers;
    protected int n_iter;
    protected Double eta;
    protected boolean visualize = false;
    protected int visual_sleep = 200;

    public void setVisualizationMode(boolean res) {
        this.visualize = res;
    }

    public void setEta(Double eta) {
        this.eta = eta;
    }

    public void setNIter(int n_iter) {
        this.n_iter = n_iter;
    }

    public Double getEta() {
        return this.eta;
    }

    public int getNIter() {
        return this.n_iter;
    }

    public ArrayList<NeuralLayer> getLayers() {
        return this.layers;
    }

    protected final void randomWeights(Double min, Double max) {
        double random;
        for (NeuralLayer layer : this.layers) {
            for (int i = 0; i < layer.size(); i++) {
                layer.getNeuron(i).getConnections().replaceAll((k, v) -> ThreadLocalRandom.current().nextDouble(min, max));
            }
        }
    }

    public DataFrame predict(DataObject data) {
        DataFrame predictions = new DataFrame();
        for (int i = 0; i < data.getCount(); i++) {
            this.clearLayersValues();
            Double pred = this.getPredict(data.getFrameByIndex(i));
            predictions.pushValue(pred);
        }
        return predictions;
    }

    protected void inputInit(DataFrame input) {
        for (int i = 0; i < this.layers.get(0).size(); i++) {
            try {
                this.layers.get(0).getNeuron(i).setValue((Double) input.getValueByIndex(i));
            } catch (IndexOutOfBoundsException e) {
                this.layers.get(0).getNeuron(i).setValue(1.);
            }
        }
    }

    protected final void clearLayersValues() {
        for (NeuralLayer layer : this.layers) {
            layer.clearValues();
        }
    }

    public void fit(DataObject src, DataObject dst) {
        for (int iter = 0; iter < this.n_iter; iter++) {
            for (int i = 0; i < src.getCount(); i++) {
                this.clearLayersValues();
                Double error = (Double) dst.getFrameByIndex(i).getValueByIndex(0) - this.getPredict(src.getFrameByIndex(i));
                error = error * this.eta;
                this.inputInit(src.getFrameByIndex(i));
                for (int layerno = 0; layerno < this.layers.size(); layerno++) {
                    for (int neuronno = 0; neuronno < this.layers.get(layerno).size(); neuronno++) {
                        Neuron current_neuron = this.layers.get(layerno).getNeuron(neuronno);
                        HashMap<HashMap<Integer, Integer>, Double> conns = current_neuron.getConnections();
                        for (HashMap<Integer, Integer> index : conns.keySet()) {
                            for (Integer connneur : index.keySet()) {
                                Integer layer = index.get(connneur);
                                current_neuron.updateWeight(connneur, layer, error * current_neuron.getValue());
                                Double output = current_neuron.getOutput(connneur, layer);
                                Neuron connected = this.layers.get(layer).getNeuron(connneur);
                                connected.setValue(connected.getValue() + output);
                            }
                        }
                    }
                }
                if (this.visualize) {
                    try {
                        Thread.sleep(this.visual_sleep);
                    } catch (InterruptedException e) {
                        System.err.println("There is a problem with neural thread");
                    }

                }
            }
        }
        this.clearLayersValues();
    }

    public Double getPredict(DataFrame X) {
        this.inputInit(X);
        for (int layerno = 0; layerno < this.layers.size(); layerno++) {
            for (int neuronno = 0; neuronno < this.layers.get(layerno).size(); neuronno++) {
                Neuron current_neuron = this.layers.get(layerno).getNeuron(neuronno);
                HashMap<HashMap<Integer, Integer>, Double> conns = current_neuron.getConnections();
                for (HashMap<Integer, Integer> index : conns.keySet()) {
                    for (Integer connneur : index.keySet()) {
                        Integer layer = index.get(connneur);
                        Double output = current_neuron.getOutput(connneur, layer);
                        Neuron connected = this.layers.get(layer).getNeuron(connneur);
                        connected.setValue(connected.getValue() + output);
                    }
                }
            }
        }
        return ((OutputLayer) this.layers.get(this.layers.size() - 1)).getPrediction();
    }

    public Double getLinearPredict(DataFrame X) {
        this.inputInit(X);
        for (int layerno = 0; layerno < this.layers.size(); layerno++) {
            for (int neuronno = 0; neuronno < this.layers.get(layerno).size(); neuronno++) {
                Neuron current_neuron = this.layers.get(layerno).getNeuron(neuronno);
                HashMap<HashMap<Integer, Integer>, Double> conns = current_neuron.getConnections();
                for (HashMap<Integer, Integer> index : conns.keySet()) {
                    for (Integer connneur : index.keySet()) {
                        Integer layer = index.get(connneur);
                        Double output = current_neuron.getOutput(connneur, layer);
                        Neuron connected = this.layers.get(layer).getNeuron(connneur);
                        connected.setValue(connected.getValue() + output);
                    }
                }
            }
        }
        //this.layers.get(this.layers.size()-1).print();
        return ((OutputLayer) this.layers.get(this.layers.size() - 1)).getLinearPrediction();
    }

    public void setVisualSleepTime(int time) {
        this.visual_sleep = time;
    }
}

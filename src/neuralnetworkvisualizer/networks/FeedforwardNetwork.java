/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetworkvisualizer.networks;

import java.util.ArrayList;
import neuralnetworkvisualizer.interfaces.Model;
import neuralnetworkvisualizer.neurons.NeuralLayer;

/**
 *
 * @author Jacek
 */
public final class FeedforwardNetwork extends NeuralNetwork  implements Model {

    private FeedforwardNetwork(FeedforwardBuilder builder) {
        this.layers = builder.layers;
        this.n_iter = builder.n_iter;
        this.eta = builder.eta;
        this.randomWeights(-1., 1.);
    }

    public static class FeedforwardBuilder extends NetworkBuilder {

        private ArrayList<NeuralLayer> layers;
        private int n_iter;
        private Double eta;

        public FeedforwardBuilder(Double eta, int n_iter) {
            super(eta, n_iter);
            this.layers = super.layers;
            this.eta = super.eta;
            this.n_iter = super.n_iter;
        }

        public FeedforwardBuilder addInputLayer(int nodes) {
            return (FeedforwardBuilder) super.addInputLayer(nodes);
        }

        public FeedforwardBuilder addHiddenLayer(int nodes) {
            return (FeedforwardBuilder) super.addHiddenLayer(nodes);
        }

        public FeedforwardBuilder addOutputLayer() {
            return (FeedforwardBuilder) super.addOutputLayer();
        }

        public FeedforwardBuilder connectFully() {
            for (int i = 0; i < this.layers.size() - 1; i++) {
                this.layers.get(i).connectFull(this.layers.get(i + 1));
            }
            return this;
        }

        public FeedforwardNetwork build() {
            return new FeedforwardNetwork(this);
        }

    }

}

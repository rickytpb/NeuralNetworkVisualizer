/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetworkvisualizer.networking;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import neuralnetworkvisualizer.utils.DataFrame;
import neuralnetworkvisualizer.utils.DataObject;
import neuralnetworkvisualizer.utils.exceptions.ServerNotAccessibleException;

/**
 *
 * @author Jacek
 */
public class PredictionClient {

    public static DataFrame getPredictions(DataObject X, String ip, int portno) throws ServerNotAccessibleException {
        DataFrame df = new DataFrame();
        try {
            Socket s = new Socket(ip, portno);
            OutputStream os = s.getOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(os);
            oos.writeObject(X);

            InputStream is = s.getInputStream();
            ObjectInputStream ois = new ObjectInputStream(is);
            df = (DataFrame) ois.readObject();
            oos.close();
            os.close();
            s.close();
        } catch (IOException | ClassNotFoundException e) {
            throw new ServerNotAccessibleException("Server not responding");
        }
        return df;
    }
}

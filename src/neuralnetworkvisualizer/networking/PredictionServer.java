/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetworkvisualizer.networking;

import neuralnetworkvisualizer.networks.NeuralNetwork;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import neuralnetworkvisualizer.utils.DataObject;

/**
 *
 * @author Jacek
 */
public class PredictionServer extends Thread implements Runnable {

    NeuralNetwork net;
    int portno;
    ServerSocket listener;
    Thread t;

    public PredictionServer(int port, NeuralNetwork net) {
        this.portno = port;
        this.net = net;
    }

    public void closeSocket() {
        try {
            listener.close();
        } catch (IOException e) {
            System.err.println("Caught");
        }
    }

    @Override
    public void run() {
        try {
            this.listener = new ServerSocket(this.portno);
            while (true) {
                Socket s = listener.accept();
                InputStream is = s.getInputStream();
                ObjectInputStream ois = new ObjectInputStream(is);
                DataObject to = (DataObject) ois.readObject();
                OutputStream os = s.getOutputStream();
                ObjectOutputStream oos = new ObjectOutputStream(os);
                oos.writeObject(net.predict(to));
                ois.close();
                is.close();
                s.close();
            }
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("Server was killed");
        }
    }

    public void start() {
        if (t == null) {
            t = new Thread(this);
            t.start();
        }
    }
}

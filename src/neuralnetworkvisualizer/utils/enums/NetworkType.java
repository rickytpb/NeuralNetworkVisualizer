/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetworkvisualizer.utils.enums;

/**
 *
 * @author Jacek
 */
public enum NetworkType {
    FEEDFORWARD, RECURRENT, PERCEPTRON, ADALINE;
}

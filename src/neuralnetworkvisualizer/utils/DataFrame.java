/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetworkvisualizer.utils;

import java.util.ArrayList;
import java.util.List;
import java.io.Serializable;
import neuralnetworkvisualizer.interfaces.DataInterface;

/**
 *
 * @author Jacek
 */
public class DataFrame implements Serializable, DataInterface {

    private int count;
    private List<Object> dataset;

    public DataFrame() {
        this.count = 0;
        this.dataset = new ArrayList<Object>();
    }

    public DataFrame(String[] content) {
        this.count = 0;
        this.dataset = new ArrayList<Object>();
        for (String item : content) {
            dataset.add(item);
            this.count++;
        }
    }

    public DataFrame(Object obj) {
        this.count = 1;
        this.dataset = new ArrayList<Object>();
        this.dataset.add(obj);
    }

    public DataFrame(Float[] content) {
        this.count = 0;
        this.dataset = new ArrayList<Object>();
        for (Float item : content) {
            dataset.add(item);
            this.count++;
        }
    }

    public void pushValue(Object obj) {
        this.dataset.add(obj);
    }

    public Object getValueByIndex(int index) {
        return this.dataset.get(index);
    }

    public int getCount() {
        return this.dataset.size();
    }

    public boolean isEqual(DataFrame x) {
        if (x.getCount() != this.getCount()) {
            return false;
        }
        for (int i = 0; i < x.getCount(); i++) {
            if (Double.compare((Double) x.getValueByIndex(i), (Double) this.getValueByIndex(i)) != 0) {
                return false;
            }
        }
        return true;
    }

    public double getDiffPercentage(DataFrame X) {
        //EXCEPTION NOT ALIGNED
        int errors = 0;
        for (int i = 0; i < X.getCount(); i++) {
            if (Double.compare((Double) X.getValueByIndex(i), (Double) this.getValueByIndex(i)) != 0) {
                errors += 1;
            }
        }
        Double rate = errors / (double) X.getCount();
        return rate;
    }

    @Override
    public String toString() {
        String content = "";
        for (Object str : dataset) {
            content += str;
            content += " ";
        }
        return content;
    }

    public void print() {
        for (int i = 0; i < this.getCount(); i++) {
            System.out.format("%.1f ", this.getValueByIndex(i));
        }
        System.out.print("\n");
    }
}

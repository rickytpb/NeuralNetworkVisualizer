/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetworkvisualizer.utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import neuralnetworkvisualizer.utils.DataReader;
import neuralnetworkvisualizer.utils.DataObject;
import neuralnetworkvisualizer.utils.DataFrame;
import java.util.List;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import neuralnetworkvisualizer.networks.FeedforwardNetwork;
import neuralnetworkvisualizer.networks.NeuralNetwork;
import neuralnetworkvisualizer.networks.RecurrentNetwork;
import neuralnetworkvisualizer.networks.NetworkBuilder;
import neuralnetworkvisualizer.networks.Perceptron;
import neuralnetworkvisualizer.networks.AdaptivePerceptron;
import neuralnetworkvisualizer.networking.PredictionServer;
import neuralnetworkvisualizer.networking.PredictionClient;
import neuralnetworkvisualizer.utils.exceptions.*;
import neuralnetworkvisualizer.utils.ConfigParser;
import neuralnetworkvisualizer.utils.enums.*;
import neuralnetworkvisualizer.visuals.GraphDraw;

/**
 *
 * @author Jacek
 */
public class Controller {

    DataObject full_data;
    DataObject train_data;
    DataObject test_data;

    NeuralNetwork network_one;
    NeuralNetwork network_two;

    DataFrame preds1;
    DataFrame preds2;
    DataFrame preds_remote;

    PredictionServer serv1;
    PredictionServer serv2;

    GraphDraw drawer1;
    GraphDraw drawer2;

    private static class ControllerHolder {

        private final static Controller instance = new Controller();
    }

    public static Controller getInstance() {
        return ControllerHolder.instance;
    }

    public void startServer(int id, int portno) {
        if (id == 1) {
            serv1 = new PredictionServer(portno, this.network_one);
            serv1.start();
        }
        if (id == 2) {
            serv2 = new PredictionServer(portno, this.network_two);
            serv2.start();
        }
    }

    public void setNetworkVisualizeSleepTime(int id, int time) {
        if (id == 1) {
            this.network_one.setVisualSleepTime(time);

        }
        if (id == 2) {
            this.network_two.setVisualSleepTime(time);
        }
    }

    public void stopServer(int id) {
        if (id == 1) {
            serv1.closeSocket();
            serv1 = null;
        }
        if (id == 2) {
            serv2.closeSocket();
            serv2 = null;
        }
    }

    public boolean isDataLoaded() {
        return this.test_data != null && this.train_data != null;
    }

    public boolean isNetOneSet() {
        return this.network_one != null;
    }

    public boolean isNetTwoSet() {
        return this.network_two != null;
    }

    public Double getNetOneEta() {
        return this.network_one.getEta();
    }

    public Double getNetTwoEta() {
        return this.network_two.getEta();
    }

    public int getNetOneNIter() {
        return this.network_one.getNIter();
    }

    public int getNetTwoNIter() {
        return this.network_two.getNIter();
    }

    public double predictRemote(String ip, int portno) {
        DataObject X = this.test_data.splitByColumns(getFeatureColumns(this.test_data.getFrameCount()));
        try {
            preds_remote = PredictionClient.getPredictions(X, ip, portno);
        } catch (ServerNotAccessibleException e) {
            return -1.;
        }
        DataObject Y = this.test_data.splitByColumns(new int[]{this.test_data.getFrameCount() - 1});
        return preds_remote.getDiffPercentage(Y.extractColumnAsFrame(0));
    }

    public void showNetwork(int net_id) {
        Thread temp = new Thread(new Runnable() {
            @Override
            public void run() {
                NeuralNetwork temp_net;
                temp_net = network_one;
                if (net_id == 2) {
                    temp_net = network_two;
                }

                drawer1 = new GraphDraw();
                drawer1.createGraph(temp_net);
                drawer1.display();
                drawer1.updateGraph(temp_net);
                drawer1 = null;
            }
        });
        temp.start();
    }

    public void setNetworkOne(NeuralNetwork net) {
        network_one = net;
    }

    public void setNetworkTwo(NeuralNetwork net) {
        network_two = net;
    }

    public void setNetOneProperties(Double eta, int n_iter) {
        if (network_one == null) {
            return;
        }
        network_one.setEta(eta);
        network_one.setNIter(n_iter);
    }

    public void setNetTwoProperties(Double eta, int n_iter) {
        if (network_two == null) {
            return;
        }
        network_two.setEta(eta);
        network_two.setNIter(n_iter);
    }

    public Double getNetOneErrorRate() {
        DataObject X = this.test_data.splitByColumns(getFeatureColumns(this.test_data.getFrameCount()));
        preds1 = this.network_one.predict(X);
        DataObject Y = this.test_data.splitByColumns(new int[]{this.test_data.getFrameCount() - 1});
        return preds1.getDiffPercentage(Y.extractColumnAsFrame(0));
    }

    public Double getNetTwoErrorRate() {
        if (this.network_two == null) {
            return -1.;
        }
        DataObject X = this.test_data.splitByColumns(getFeatureColumns(this.test_data.getFrameCount()));
        preds2 = this.network_two.predict(X);
        DataObject Y = this.test_data.splitByColumns(new int[]{this.test_data.getFrameCount() - 1});
        return preds2.getDiffPercentage(Y.extractColumnAsFrame(0));
    }

    public String getPredictionString() {
        String retString = "";
        if (this.preds1 != null) {
            retString += "Network one predictions: \n";
            retString += this.preds1.toString();
            retString += "\n\n";
        }
        if (this.preds2 != null) {
            retString += "Network two predictions: \n";
            retString += this.preds2.toString();
            retString += "\n\n";
        }
        if (this.preds_remote != null) {
            retString += "Remote network predictions: \n";
            retString += this.preds_remote.toString();
            retString += "\n\n";
        }
        retString += "Actual labels: \n";
        retString += this.test_data.extractColumnAsFrame(this.test_data.getFrameCount() - 1);
        return retString;
    }

    private int[] getFeatureColumns(int size) {
        int[] arr = new int[size - 1];
        for (int i = 0; i < size - 1; i++) {
            arr[i] = i;
        }
        return arr;
    }

    public void fitNetworks(boolean visualize_one) {
        if (this.train_data == null) {
            return;
        }
        DataObject X = this.train_data.splitByColumns(getFeatureColumns(this.train_data.getFrameCount()));
        DataObject Y = this.train_data.splitByColumns(new int[]{this.train_data.getFrameCount() - 1});
        if (this.network_one != null) {
            if (visualize_one) {
                this.network_one.setVisualizationMode(visualize_one);
            }
            Thread none = new Thread(new Runnable() {
                @Override
                public void run() {
                    network_one.fit(X, Y);
                }
            });
            none.start();

            if (visualize_one) {
                Thread temp = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        drawer1 = new GraphDraw();
                        drawer1.createGraph(network_one);
                        drawer1.display();
                        while (none.isAlive()) {
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                System.err.println(e.getMessage());
                            }
                            drawer1.updateGraph(network_one);
                        }
                        drawer1 = null;
                    }
                });
                temp.start();
            }
            try {
                none.join();

            } catch (InterruptedException e) {
                System.err.println(e.getMessage());
            }
        }

        if (this.network_two != null) {
            Thread none = new Thread(new Runnable() {
                @Override
                public void run() {
                    network_two.fit(X, Y);
                }
            });
            none.start();
            try {
                none.join();

            } catch (InterruptedException e) {
                System.err.println(e.getMessage());
            }

        }
    }

    public NeuralNetwork createNetwork(String cfg, Double eta, int n_iter) throws IOException {
        ConfigParser pars = new ConfigParser();
        try {
            pars.parseConfig(cfg);
        } catch (WrongNetworkTypeException e) {
            throw new IOException("Provide a valid config!");
        }
        NetworkBuilder temp_builder;
        NetworkType type = pars.getNetworkType();
        switch (type) {
            case FEEDFORWARD:
                temp_builder = new FeedforwardNetwork.FeedforwardBuilder(eta, n_iter);
                break;
            case RECURRENT:
                temp_builder = new RecurrentNetwork.RecurrentBuilder(eta, n_iter);
                break;
            case PERCEPTRON:
                temp_builder = new Perceptron.PerceptronBuilder(eta, n_iter);
                break;
            case ADALINE:
                temp_builder = new AdaptivePerceptron.AdaptivePerceptronBuilder(eta, n_iter);
                break;
            default:
                return null;
        }
        temp_builder.addInputLayer(pars.getInputNeurons());
        List<Integer> hls = pars.getHiddenLayers();
        for (int i = 0; i < hls.size(); i++) {
            temp_builder.addHiddenLayer(hls.get(i));
        }
        return temp_builder.addOutputLayer().connectFully().build();
    }

    public void loadData(String filename) throws IOException {
        DataReader reader = new DataReader();
        try {
            this.full_data = reader.readFromCsvString(filename, ",");
        } catch (DataNotFoundException e) {
            System.err.println(e.getMessage());
            throw new IOException();
        }
    }

    public void trainTestSplit(Double test_size, boolean random) {
        if (Double.compare(test_size, 0.0) <= 0 || Double.compare(test_size, 1.0) >= 0) {
            this.test_data = this.full_data;
            this.train_data = this.full_data;
            return;
        }

        List<DataObject> samples = this.full_data.trainTestSplit(test_size, random);
        this.test_data = samples.get(0);
        this.train_data = samples.get(1);
    }

    public DataObject getFullData() {
        return this.full_data;
    }

    public void saveNetworkAsFile(String fname, int netid) {
        try {
            FileOutputStream fout = new FileOutputStream(fname);
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            if (netid == 1) {
                oos.writeObject(this.network_one);
            }
            if (netid == 2) {
                oos.writeObject(this.network_two);
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    public boolean loadNetworkFromFile(String fname, int netid) {
        try {
            FileInputStream fin = new FileInputStream(fname);
            ObjectInputStream ois = new ObjectInputStream(fin);
            if (netid == 1) {
                try {
                    this.network_one = (NeuralNetwork) ois.readObject();
                } catch (ClassNotFoundException e) {
                    System.err.println(e.getMessage());
                    return false;
                }
            }

            if (netid == 2) {
                try {
                    this.network_two = (NeuralNetwork) ois.readObject();
                } catch (ClassNotFoundException e) {
                    System.err.println(e.getMessage());
                    return false;
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

}

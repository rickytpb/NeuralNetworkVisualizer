/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetworkvisualizer.utils.exceptions;

/**
 *
 * @author Jacek
 */
public class WrongNetworkTypeException extends Exception {

    public WrongNetworkTypeException(String message) {
        super(message);
    }

}

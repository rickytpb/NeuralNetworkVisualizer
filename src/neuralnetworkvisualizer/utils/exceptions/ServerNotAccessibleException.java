/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetworkvisualizer.utils.exceptions;

/**
 *
 * @author Jacek
 */
public class ServerNotAccessibleException extends Exception {

    public ServerNotAccessibleException(String message) {
        super(message);
    }
}

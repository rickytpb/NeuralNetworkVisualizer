/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetworkvisualizer.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;
import neuralnetworkvisualizer.utils.exceptions.DataNotFoundException;
import neuralnetworkvisualizer.utils.DataObject;

/**
 *
 * @author Jacek
 */
public class DataReader {

    public DataObject readFromCsvString(String fname, String sep) throws DataNotFoundException {
        DataObject data = new DataObject();
        DataFrame frame;

        try {
            String line = "";
            BufferedReader br = new BufferedReader(new FileReader(fname));
            while ((line = br.readLine()) != null) {

                frame = new DataFrame();
                String[] str_frame = line.split(sep);

                for (String str : str_frame) {
                    try {
                        double num = Double.parseDouble(str);
                        frame.pushValue(num);
                    } catch (NumberFormatException nfe) {
                        throw new DataNotFoundException("File contains malformed data");
                    }
                }
                data.pushFrame(frame);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (data.getCount() < 1) {
            throw new DataNotFoundException("File did not contain any data");
        }
        return data;
    }
}

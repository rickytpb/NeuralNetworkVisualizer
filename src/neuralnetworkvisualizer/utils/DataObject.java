/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetworkvisualizer.utils;

import java.util.List;
import java.util.ArrayList;
import neuralnetworkvisualizer.utils.DataFrame;
import java.util.Collections;
import java.io.Serializable;
import neuralnetworkvisualizer.interfaces.DataInterface;

/**
 *
 * @author Jacek
 */
public class DataObject implements Serializable, DataInterface {

    private List<DataFrame> dataset;
    private Integer count;

    public DataObject() {
        this.dataset = new ArrayList<DataFrame>();
        this.count = 0;
    }

    public void pushFrame(DataFrame frame) {
        this.dataset.add(frame);
        this.count++;
    }

    public void deleteFrame(int frameno) {
        this.dataset.remove(frameno);
    }

    public DataFrame getFrameByIndex(Integer frameno) {
        return this.dataset.get(frameno);
    }

    public int getCount() {
        return this.count;
    }

    public int getFrameCount() {
        return this.dataset.get(0).getCount();
    }

    public DataObject splitByColumns(int[] args) {
        DataObject splitted = new DataObject();
        for (DataFrame df : this.dataset) {
            DataFrame new_df = new DataFrame();
            for (int col : args) {
                new_df.pushValue(df.getValueByIndex(col));
            }
            splitted.pushFrame(new_df);
        }
        return splitted;
    }

    public DataFrame extractColumnAsFrame(int index) {
        DataFrame retdf = new DataFrame();
        for (DataFrame df : this.dataset) {
            retdf.pushValue(df.getValueByIndex(index));
        }
        return retdf;
    }

    @Override
    public void print() {
        for (DataFrame frame : this.dataset) {
            for (int i = 0; i < frame.getCount(); i++) {
                System.out.format("%.1f ", frame.getValueByIndex(i));
            }
            System.out.print("\n");
        }
        System.out.print("\n");
    }

    public List<DataObject> trainTestSplit(Double test_size, boolean random) {
        int test_s = new Double(test_size * (new Double(this.dataset.size()))).intValue();
        int train_s = this.dataset.size() - test_s;
        DataObject test_dat = new DataObject();
        DataObject train_dat = new DataObject();
        List<DataFrame> dataset_cp = this.dataset;
        if (random) {
            Collections.shuffle(dataset_cp);
        }

        for (DataFrame frame : dataset_cp) {
            if (test_s > 0) {
                test_dat.pushFrame(frame);
                test_s -= 1;
            } else {
                train_dat.pushFrame(frame);
            }

        }

        List<DataObject> return_list = new ArrayList<DataObject>();
        return_list.add(test_dat);
        return_list.add(train_dat);
        return return_list;
    }

}

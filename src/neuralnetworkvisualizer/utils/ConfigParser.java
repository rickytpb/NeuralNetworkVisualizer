/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetworkvisualizer.utils;

import java.util.List;
import java.util.ArrayList;
import neuralnetworkvisualizer.utils.exceptions.WrongNetworkTypeException;
import neuralnetworkvisualizer.utils.enums.NetworkType;

/**
 *
 * @author Jacek
 */
public class ConfigParser {

    int input_neurons;
    List<Integer> hidden_layers_neurons;
    NetworkType type;

    public ConfigParser() {
        input_neurons = 0;
        hidden_layers_neurons = new ArrayList<Integer>();
    }

    public void parseConfig(String cfg) throws WrongNetworkTypeException {
        String[] config = cfg.trim().split(";");
        switch (config[0]) {
            case "R":
                type = NetworkType.RECURRENT;
                break;
            case "F":
                type = NetworkType.FEEDFORWARD;
                break;
            case "P":
                type = NetworkType.PERCEPTRON;
                break;
            case "A":
                type = NetworkType.ADALINE;
                break;
            default:
                throw new WrongNetworkTypeException("Provide a correct letter code for network type!");
        }
        input_neurons = Integer.parseInt(config[1]);
        if (type == NetworkType.PERCEPTRON || type == NetworkType.ADALINE) {
            return;
        }
        for (int i = 2; i < config.length; i++) {
            hidden_layers_neurons.add(Integer.parseInt(config[i]));
        }

    }

    public int getInputNeurons() {
        return this.input_neurons;
    }

    public List<Integer> getHiddenLayers() {
        return this.hidden_layers_neurons;
    }

    public NetworkType getNetworkType() {
        return this.type;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetworkvisualizer.interfaces;

import neuralnetworkvisualizer.utils.DataObject;
import neuralnetworkvisualizer.utils.DataFrame;

/**
 *
 * @author Jacek
 */
public interface Model {

    public void fit(DataObject src, DataObject dst);

    public DataFrame predict(DataObject data);

    public void setVisualizationMode(boolean res);
}

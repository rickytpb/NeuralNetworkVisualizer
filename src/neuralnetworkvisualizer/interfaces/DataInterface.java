/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetworkvisualizer.interfaces;

/**
 *
 * @author Jacek
 */
public interface DataInterface {

    @Override
    public String toString();

    public int getCount();

    public void print();
}

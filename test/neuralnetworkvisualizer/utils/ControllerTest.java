/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetworkvisualizer.utils;

import java.io.IOException;
import java.net.Socket;
import java.nio.file.Paths;
import java.nio.file.Files;
import neuralnetworkvisualizer.networks.NeuralNetwork;
import neuralnetworkvisualizer.networks.Perceptron;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jacek
 */
public class ControllerTest {

    Controller instance;

    public ControllerTest() {
        instance = new Controller();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

        instance = Controller.getInstance();
        instance.network_one = (NeuralNetwork) (new Perceptron.PerceptronBuilder(0.01, 1)
                .addInputLayer(2)
                .addOutputLayer()
                .connectFully()
                .build());
        instance.network_two = (NeuralNetwork) (new Perceptron.PerceptronBuilder(0.01, 1)
                .addInputLayer(2)
                .addOutputLayer()
                .connectFully()
                .build());
    }

    @After
    public void tearDown() {
        instance = null;
    }

    /**
     * Test of getInstance method, of class Controller.
     */
    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        Controller expResult = null;
        Controller result = Controller.getInstance();
        assertNotEquals(expResult, result);
    }

    /**
     * Test of startServer method, of class Controller.
     */
    @Test
    public void testStartServer() {
        System.out.println("startServer");
        int id = 2;
        int portno = 9222;
        Controller instance = new Controller();
        instance.startServer(id, portno);
        try {
            Socket s = new Socket("localhost", 9222);
        } catch (IOException e) {
            assertNotEquals(e, null);
        }
        instance.stopServer(id);
    }

    /**
     * Test of stopServer method, of class Controller.
     */
    @Test
    public void testStopServer() {
        System.out.println("stopServer");
        int id = 1;
        Controller instance = new Controller();
        instance.startServer(id, 9222);
        try {
            Socket s = new Socket("localhost", 9222);
        } catch (IOException e) {
            assertNotEquals(e, null);
        }
        instance.stopServer(id);
        assertEquals(instance.serv1, null);

        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of isDataLoaded method, of class Controller.
     */
    @Test
    public void testIsDataLoaded() {
        System.out.println("isDataLoaded");
        Controller instance = new Controller();
        boolean expResult = false;
        boolean result = instance.isDataLoaded();
        assertEquals(expResult, result);
    }

    /**
     * Test of setNetworkOne method, of class Controller.
     */
    @Test
    public void testSetNetworkOne() {
        System.out.println("setNetworkOne");
        NeuralNetwork net = null;
        Controller instance = new Controller();
        instance.setNetworkOne(net);
        assertEquals(instance.isNetTwoSet(), false);
    }

    /**
     * Test of setNetworkTwo method, of class Controller.
     */
    @Test
    public void testSetNetworkTwo() {
        System.out.println("setNetworkTwo");
        NeuralNetwork net = null;
        Controller instance = new Controller();
        instance.setNetworkTwo(net);
        assertEquals(instance.isNetOneSet(), false);
    }

    /**
     * Test of setNetOneProperties method, of class Controller.
     */
    @Test
    public void testSetNetOneProperties() {
        System.out.println("setNetOneProperties");
        Double eta = 0.5;
        int n_iter = 5;
        instance.setNetOneProperties(eta, n_iter);
        assertEquals(instance.getNetOneEta(), eta);
        assertEquals(instance.getNetOneNIter(), n_iter);
    }

    /**
     * Test of setNetTwoProperties method, of class Controller.
     */
    @Test
    public void testSetNetTwoProperties() {
        System.out.println("setNetTwoProperties");
        Double eta = 0.5;
        int n_iter = 5;
        instance.setNetTwoProperties(eta, n_iter);
        assertEquals(instance.getNetTwoEta(), eta);
        assertEquals(instance.getNetTwoNIter(), n_iter);
    }

    /**
     * Test of getNetOneErrorRate method, of class Controller.
     */
    @Test
    public void testGetNetOneErrorRate() {
        System.out.println("getNetOneErrorRate");
        Double expResult = null;
        try {
            instance.loadData("test/resources/iris_small.csv");
        } catch (IOException e) {
            fail("Data not loaded");
        }
        instance.trainTestSplit(1., true);
        Double result = instance.getNetOneErrorRate();
        System.out.println(result);
        assertNotEquals(result, expResult);

    }

    /**
     * Test of getNetTwoErrorRate method, of class Controller.
     */
    @Test
    public void testGetNetTwoErrorRate() {
        System.out.println("getNetTwoErrorRate");
        Double expResult = null;
        try {
            instance.loadData("test/resources/iris_small.csv");
        } catch (IOException e) {
            fail("Data not loaded");
        }
        instance.trainTestSplit(1., true);
        Double result = instance.getNetTwoErrorRate();
        System.out.println(result);
        assertNotEquals(result, expResult);
    }

    /**
     * Test of fitNetworks method, of class Controller.
     */
    @Test
    public void testFitNetworks() {
        System.out.println("fitNetworks");
        boolean visualize_one = false;
        Controller instance = new Controller();
        instance.fitNetworks(visualize_one);
    }

    /**
     * Test of createNetwork method, of class Controller.
     */
    @Test
    public void testCreateNetwork() throws Exception {
        System.out.println("createNetwork");
        String cfg = "P;3";
        Double eta = null;
        int n_iter = 0;
        Controller instance = new Controller();
        NeuralNetwork result = instance.createNetwork(cfg, eta, n_iter);
        assertEquals(Perceptron.class, result.getClass());
    }

    /**
     * Test of loadData method, of class Controller.
     */
    @Test
    public void testLoadData() throws Exception {
        System.out.println("loadData");
        String filename = "dummyfile";
        Controller instance = new Controller();
        instance.loadData("test/resources/iris_small.csv");
        assertNotEquals(instance.full_data, null);
    }

    /**
     * Test of trainTestSplit method, of class Controller.
     */
    @Test
    public void testTrainTestSplit() {
        System.out.println("trainTestSplit");
        Double test_size = 0.2;
        boolean random = true;
        try {
            instance.loadData("test/resources/iris_small.csv");
        } catch (IOException e) {
            fail("Data not loaded!");
        }
        instance.trainTestSplit(test_size, random);
        assertEquals(instance.isDataLoaded(), true);
    }

    /**
     * Test of getFullData method, of class Controller.
     */
    @Test
    public void testGetFullData() {
        System.out.println("getFullData");
        DataObject expResult = null;
        try {
            instance.loadData("test/resources/iris_small.csv");
        } catch (IOException e) {
            fail("No data loaded");
        }
        DataObject result = instance.getFullData();
        assertNotEquals(expResult, result);
    }

    /**
     * Test of saveNetworkAsFile method, of class Controller.
     */
    @Test
    public void testSaveNetworkAsFile() {
        System.out.println("saveNetworkAsFile");
        String fname = "test/resources/temp.obj";
        int netid = 1;
        String cfg = "F;3;4;2";
        try {
            instance.createNetwork(cfg, 0.1, netid);
        } catch (IOException e) {
            fail("Network not created");
        }

        instance.saveNetworkAsFile(fname, netid);
        try {
            Files.deleteIfExists(Paths.get(fname));
        } catch (IOException e) {
            System.out.println("Problem with deletion - test still is ok");
        }

    }

    /**
     * Test of loadNetworkFromFile method, of class Controller.
     */
    @Test
    public void testLoadNetworkFromFile() {
        System.out.println("loadNetworkFromFile");
        String fname = "test/resources/net1.obj";
        int netid = 1;
        boolean result = instance.loadNetworkFromFile(fname, netid);
        assertNotEquals(instance.network_one, null);
    }

}
